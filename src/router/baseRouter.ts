const baseRouter = [
  {
    path: "/",
    component: () => import("../views/article/index.vue"),
        redirect:'/category/all',
        children:[
      {
        path: "/category/:id",
        component: () => import("../views/category/index.vue"),
        meta: {
          title: "文章分类",
        },
      },
    ],
    meta: {
      title: "文章",
      nav: true,
    },
  },
  {
    path: "/archives",
    component: () => import("../views/archives/index.vue"),
    meta: {
      title: "归档",
      nav: true,
    },
  },
  {
    path: "/knowledge",
    component: () => import("../views/knowledge/index.vue"),
    meta: {
      title: "知识小册",
      nav: true,
    },
  },
  {
    path: "/page/:id",
    component: () => import("../views/page/index.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/detail/:id",
    component: () => import("../views/knowledgeDetail/index.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/articalDetail/:id",
    component: () => import("../views/articalDetail/index.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/TagDetail/:id",
    component: () => import("../views/TagDetail/index.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/search",
    component: () => import("../views/search/index.vue"),
    meta: {
      nav: false,
    },
  },
];
export default baseRouter;
