import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import baseRouter from "./baseRouter";
const routes: Array<RouteRecordRaw> = [...baseRouter];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
