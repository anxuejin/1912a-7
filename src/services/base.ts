import http from "@/utils/httpTool";
import { Params } from "@/utils/httpTool";
//三金页面路由
export const getNavPage = (params: Params = {}) =>
  http.get("/api/page", params);
  //文章页面路由
export const getArtical = (params: Params = {}) =>
  http.get("/api/article", params);
//文章标签
export const getTag = (params: Params = {}) => http.get("/api/tag", params);
//知识小册页面路由
export const getNavKnowledge = (params: Params = {}) =>
  http.get("/api/knowledge", params);
//推荐阅读
export const getReadList = (params: Params = {}) =>
  http.get("/api/article/recommend");
//文章分类
export const getTypeList = (params: Params = {}) =>
  http.get("/api/category", {
    articleStatus: "publish",
  });
//文章分类获取对应的
export const getArticalCategory = (category: string, params: Params) => {
  params = category === 'all' ? params :{...params, category}
  return http.get("/api/article", params)
}

//文章详情
export const getDetailData = (id: string) => {
  return http.post("/api/article/" + id + "/views")
}

//评论接口
export const getCommentList = (
  id: string,
  params: Params = { page: 1, pageSize: 6 }
) => http.get("/api/comment/host/" + id, params)