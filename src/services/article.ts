import http from "@/utils/httpTool"
import { Params } from "@/utils/httpTool"
export const getCommentList = (
  id: string,
  params: Params = { page: 1, pageSize: 6 }
) => http.get("/api/comment/host/" + id, params)
export const sendComment = (data: any) => {
  http.post("/api/comment", data)
}
