const statusCodeMap: any = {
  "400": "您输入的参数有问题",
  "401": "您输入的身份不明确或者登录态过期，请重新登录",
  "403": "您暂无权限",
  "500": "服务器异常",
};
export default statusCodeMap;
