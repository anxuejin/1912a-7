import { reactive, onMounted } from "vue"
const useUser = () => {
  const user = reactive({
    name: "",
    email: "",
    islogin: localStorage.getItem("user") ? true : false,
  })
  try {
    const userInfo = JSON.parse(window.localStorage.getItem("user") || "")
    console.log(userInfo.name,"userInfo");
    if (userInfo) {
      user.name = userInfo.name;
      user.email = userInfo.email;
      user.islogin = true
    } else {
      user.islogin = false
    }
  } catch (error) {
    console.error(error)
  }
  return user
}
export default useUser
