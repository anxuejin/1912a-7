import { ComputedOptions } from "vue";
const icons = require.context("./icons", true, /\.vue$/); //编译时运行 不能写在if语句中
interface Components {
  [propertyName: string]: ComputedOptions;
}
const res = icons.keys().reduce((returnVal: Components, item) => {
  const components = icons(item).default;
  returnVal[components.__name] = components;
  return returnVal;
}, {});

export default res;
