import { getNavKnowledge } from "@/services/base";
import { Params } from "@/utils/httpTool";
import { defineStore } from "pinia";
import { base } from "../services";

const useStore = defineStore<string, any>("baseStore", {
  state: () => ({
    navList: [],
    articalList: [],
    knowledgeList: [],
    //推荐阅读数组
    readList: [],
    //文章类型数组
    knowledgeTypeList: [],
    //文章标签数组
    articleTagList: [],
    lists: [[], 0],
    articalDetails:[],
    itemData:[]
  }),
  actions: {
    async getNavData() {
      const { data } = await base.getNavPage();
      this.$patch({
        navList: data[0]
          .filter((item: any) => item.id)
          .sort((a: any, b: any) => b.order - a.order),
      });
    },
    async getNavKnowledge() {
      const { data } = await base.getNavKnowledge();
      console.log("data1", data[0]);
      
      this.$patch({
        knowledgeList: data[0],
      });
    },
    async getArticalData() {
      const { data } = await base.getArtical();
      console.log("getArtical", data[0]);
      this.$patch({
        articalList: data[0],
      });
      return data[0];
    },
    async getReadList() {
      const { data } = await base.getReadList();
      this.$patch({
        readList: data,
      });
      return data;
    },
    async getKnowledgeTypeList() {
      const { data } = await base.getTypeList();
      this.$patch({
        knowledgeTypeList: data,
      });
      return data;
    },
    async getArticleTagList() {
      const { data } = await base.getTag();
      this.$patch({
        articleTagList: data,
      });
      return data;
    },
    clearArticalCategoryList() {
      this.$patch({
        lists: [[], 0]
      });
    },
    async getArticalCategoryList(type: string, params: Params) {
      const { data } = await base.getArticalCategory(type, params);
      this.$patch({
        lists: [[...this.lists[0], ...data[0]], data[1]]
      });
    },
    async getArticalDetails(id: string) {
      const { data } = await base.getDetailData(id)
      this.$patch({
        articalDetails: data,
      });
      return data;
    },
  },
});

export default useStore;
