import BaseHeader from "../components/baseHeader/index.vue";
import { App } from "vue";
import icon from "../components/iconFont/index";
import BaseList from "../components/baseList/index.vue";
import comments from "../components/comments/index.vue";
import TagList from "../components/TagList/index.vue";
import baseScroll from "../components/baseScroll/index.vue";
import baseModal from "../components/baseModal/index.vue";
import likeornot from "../components/likeornot/index.vue";

export default {
  install(app: App): void {
    app.component("base-header", BaseHeader);
    app.component("base-list", BaseList);
    app.component("comments", comments);
    app.component("TagList", TagList);
    app.component("base-scorll", baseScroll);
    app.component("base-modal", baseModal);
    app.component("base-likeornot", likeornot);
    Object.keys(icon).forEach((key) => {
      app.component(key, icon[key]);
    });
  },
};
