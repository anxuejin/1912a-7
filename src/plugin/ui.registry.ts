import { App } from "vue";
import { Button, message, Input, Modal, Card, Carousel, BackTop ,Anchor,Form,Badge,Popover,Comment,Avatar} from "ant-design-vue";
const install = (app: App) => {
  app.use(Button).use(Input).use(Modal).use(Card).use(Carousel).use(BackTop).use(Anchor).use(Form).use(Badge).use(Popover).use(Comment).use(Avatar);
  app.config.globalProperties.$message = message;
};
export default {
  install,
};
