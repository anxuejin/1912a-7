import { App } from "vue"
import dayjs from "dayjs";
import relativeTime from 'dayjs/plugin/relativeTime'
import "dayjs/locale/zh-cn.js"
import "dayjs/locale/en.js"
dayjs.extend(relativeTime)
export default {
    install(app: App) {
        app.config.globalProperties.formatTime = function (val: string) {
            const locale = window.localStorage.getItem('language') || 'zh-cn'
            dayjs.locale(locale==='zh' ?'zh-cn':locale )
            return dayjs(val).fromNow(true)
        }
    }
}
