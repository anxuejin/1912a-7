import zh from "./config/zh";
import en from "./config/en";
import { createI18n } from "vue-i18n";
import { def } from "@vue/shared";
console.log(createI18n);

const messages = {
  zh,
  en,
};

const i18n = createI18n({
  locale: "zh",
  fallbackLocale: "en",
  legacy: false,
  messages,
});

export default i18n;
