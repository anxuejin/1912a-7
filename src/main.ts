import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import "./styles/common.less";
import "./styles/var.less";
import uiPlugin from "./plugin/ui.registry";
import baseComponent from "./plugin/baseComponent.registry";
import "./styles/theme.css";
import i18n from "./i18n";
import filter from "@/plugin/filters";
import 'highlight.js/styles/github.css'

createApp(App)
  .use(createPinia())
  .use(router)
  .use(filter)
  .use(uiPlugin)
  .use(i18n)
  .use(baseComponent)
  .mount("#app");
