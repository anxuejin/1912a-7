/* eslint-disable */
const path = require("path")

module.exports = {
  chainWebpack: (config) => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"]
    types.forEach((type) =>
      addStyleResource(config.module.rule("less").oneOf(type))
    )
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true,
        }
      }
    }
  },
  devServer:{
    proxy:{ //开发环境的时候 解决跨域问题
      '/api': {
        target: 'https://creationapi.shbwyz.com',
        ws: true,
        changeOrigin: true
      }
    }
  }
}

function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/styles/var.less")],
    })
}